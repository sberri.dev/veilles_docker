# Veille sur Docker

## Qu'est-ce qu'une machine virtuelle ?

Une machine virtuelle est un logiciel qui permet de créer un ordinateur virtuel à l'intérieur d'un ordinateur réel. Cela signifie qu'un système d'exploitation peut être exécuté à l'intérieur d'un autre système d'exploitation (comme Windows, Linux ou macOS)

>[!NOTE]
   >
   >Par exemple, vous pourrez exécuter un un système d'exploitation de macOS dans celui de Windows ou Linux

### Pourquoi utiliser des machines virtuelles ?

Les machines virtuelles sont utilisées pour plusieurs raisons : 

* Tester des logiciels sur les différents systèmes d'exploitation pour s'assurer de leurs bons fonctionnement.
* Utiliser des programmes qui ne sont pas compatibles avec notre système d'exploitation principal

## Qu'est-ce que Docker, et quelle est la différence avec une machine virtuelle ?

Docker est une plateforme open source qui est devenue aujourd'hui un outil indispensable pour les developpeurs. Il permet de créer, de déployer, de mettre à jour, et de gérer des applications dans des conteneurs. 

Une des principales différences entre Docker et une machine virtuelle est la façon dont les applications sont isolées. Avec une machine virtuelle, chaque application s'exécute dans son propre système d'exploitation virtuel ce qui cause une surcharge en termes de ressources et de temps de démarrage. Avec Docker, les applications s'exécutent dans des conteneurs qui partagent le même noyau du système d'exploitation, ce qui les rend plus légers et plus rapides à démarrer.

## Qu'est-ce qu'une image Docker ? 

Une image Docker est un modèle de base pour créer des conteneurs. Elle contient tous les éléments nécessaires au fonctionnement d'une application : le code source, les outils, les bibliothèques, les dépendances, les variables d'environnement et les fichiers de configuration.

Il est important de noter qu'une image Docker n'est pas exécutable par elle-même. Pour utiliser et manipuler une image, il faut créer des instances de ces images. C'est là que le conteneur entre en jeu : il représente un environnement isolé et autonome dans lequel une application peut s'exécuter.

## Qu'est-ce qu'un conteneur (container) Docker ?

Un conteneur est une instance qui provient d'une image Docker, il contient  tout ce dont une application a besoin pour fonctionner, y compris le code, les bibliothèques et les dépendances. Les conteneurs sont légers et portables, ils peuvent donc être partagés, reutilisés et même facilement déplacés d'un environnement à un autre.

Les conteneurs permettent la création, le déploiement et la gestion d'applications dans des environnements de développement, de test et de production. De plus, ils offrent une isolation des ressources cela permet l'exécution de plusieurs conteneurs sur une même machine hôte sans qu'ils n'interfèrent les uns avec les autres.

## Qu'est-ce que Docker Hub ?

Docker Hub est un service cloud qui permet de stocker, partager et gérer des images Docker. C'est une plateforme centrale où les développeurs peuvent trouver des images Docker prêtes à utiliser pour différentes applications. 

# Docker - Environnement de développement

## Les données persistent-elles lorsque qu'on supprime puis recrée un conteneur ?

Par défaut, les données ne se conservent pas lorsqu'un conteneur Docker est supprimé et recrée par la suite. En effet, toutes les modifications apportées à l'intérieur du conteneur seront perdues si le conteneur est supprimé.

## Qu'est-ce qu'un volume ?

Un volume Docker permet de stocker les données créées dans un conteneur même aprés sa suppression. Il permet également de partager des données entre les conteneurs Docker et l'hôte système.

## Qu'est-ce qu'un bind mount ? 

Un bind mount sert de lien direct entre un fichier stocké en local et un conteneur Docker. Le bind mount permet d'accéder à ce fichier sans avoir à le copier dans le conteneur. En d'autres termes, un bind mount connecte des dossiers du système hôte à un conteneur Docker.

### Quelle est la différence avec un volume et un bind volume ?

La principale différence entre un volume et un bind mount est que les volumes Docker sont stockés dans un emplacement spécifique sur le système hôte, alors que les bind mounts permettent un accès direct aux fichiers de l'hôte depuis un conteneur.
